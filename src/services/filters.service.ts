import { AxiosError } from 'axios';

import Filter from '../models/Filter.ts';
import { handleError } from '../util/util.ts';

import { axiosClient } from './service.ts';

const ENDPOINT = 'filters';

export const fetchAll = () => {
  return axiosClient.get(ENDPOINT).catch((error: AxiosError) => handleError(error));
};

export const save = (filter: Filter) => {
  return axiosClient.post(ENDPOINT, JSON.stringify(filter)).catch((error: AxiosError) => handleError(error));
};

export const update = (filter: Filter) => {
  return axiosClient.put(ENDPOINT, JSON.stringify(filter)).catch((error: AxiosError) => handleError(error));
};

export const deleteById = (filterId: number) => {
  return axiosClient.delete(`${ENDPOINT}/${filterId}`).catch((error: AxiosError) => handleError(error));
};
