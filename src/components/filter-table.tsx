import { useEffect } from 'react';
import { Button, Table } from 'react-bootstrap';

import { useAppDispatch, useAppSelector } from '../hooks.ts';
import Criterion from '../models/Criterion.ts';
import Filter from '../models/Filter.ts';
import { deleteFilterById, getAllFilters, setSelectedFilter } from '../slices/filters.slice.ts';

import '../styles/filter-table.scss';

type FilterTableProps = {
  handleShow: () => void;
  isPopUp: boolean;
};

const FilterTable = (props: FilterTableProps) => {
  const { handleShow, isPopUp } = props;
  const dispatch = useAppDispatch();
  const filters: Filter[] = useAppSelector((state) => state.filters.filters);
  const filtersStatus = useAppSelector((state) => state.filters.status);

  useEffect(() => {
    if (filtersStatus === 'idle') {
      dispatch(getAllFilters());
    }
  }, [filtersStatus, dispatch]);

  const deleteFilter = (filterId?: number) => {
    if (filterId) dispatch(deleteFilterById(filterId));
  };

  const editFilter = (filter: Filter) => {
    dispatch(setSelectedFilter(filter));
    if (isPopUp) handleShow();
  };

  return (
    <Table>
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Criteria</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {filters.map((filter: Filter, index) => (
          <tr key={filter.id}>
            <td>{index + 1}</td>
            <td>{filter.name}</td>
            <td>
              {filter.criteria.slice(0, 2).map((criterion: Criterion, index: number) => (
                <div key={criterion.id}>
                  {index +
                    1 +
                    '. ' +
                    criterion.type +
                    ' ' +
                    criterion.condition.toLowerCase() +
                    ' ' +
                    criterion.value.toLowerCase()}
                </div>
              ))}
              {filter.criteria.length > 2 ? '...' : ''}
            </td>
            <td>
              <Button className='delete-button margin-right' onClick={() => deleteFilter(filter.id)}>
                Delete
              </Button>
              <Button className='secondary-button' onClick={() => editFilter(filter)}>
                Edit
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default FilterTable;
