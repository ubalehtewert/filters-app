import { useAppSelector } from '../hooks.ts';

import FilterForm from './filter-form.tsx';

import '../styles/home-filter.scss';

const HomeFilterForm = () => {
  const selectedFilter = useAppSelector((state) => state.filters.selectedFilter);

  return (
    <div className='container'>
      <div className='home-filter-header'>
        <h3>{selectedFilter ? 'Edit filter' : 'Add new filter'}</h3>
      </div>
      <FilterForm />
    </div>
  );
};

export default HomeFilterForm;
