import { ChangeEvent, useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import DatePicker from 'react-datepicker';

import {
  AmountCriterionCondition,
  CriterionType,
  DateCriterionCondition,
  TitleCriterionCondition,
} from '../constants/enums.ts';
import { useAppDispatch, useAppSelector } from '../hooks.ts';
import Criterion from '../models/Criterion.ts';
import Filter from '../models/Filter.ts';
import { saveFilter, setSaveStatusIdle, setSelectedFilter, updateFilter } from '../slices/filters.slice.ts';

import 'react-datepicker/dist/react-datepicker.css';

export const defaultCriterion: Criterion = {
  type: CriterionType.AMOUNT,
  condition: AmountCriterionCondition.MORE,
  value: '0',
};

type FilterModalFormProps = {
  closeModal?: () => void;
};

const FilterForm = (props: FilterModalFormProps) => {
  const { closeModal } = props;
  const dispatch = useAppDispatch();
  const saveStatus = useAppSelector((state) => state.filters.saveStatus);
  const selectedFilter = useAppSelector((state) => state.filters.selectedFilter);
  const [criteria, setCriteria] = useState<Criterion[]>([defaultCriterion]);
  const [filterName, setFilterName] = useState<string>('');

  useEffect(() => {
    if (saveStatus === 'succeeded') {
      handleClose();
      dispatch(setSaveStatusIdle());
    }
  }, [dispatch, saveStatus]);

  useEffect(() => {
    setCriteria(selectedFilter?.criteria ?? [defaultCriterion]);
    setFilterName(selectedFilter?.name ?? '');
  }, [selectedFilter]);

  const handleSubmit = () => {
    if (selectedFilter) {
      const newFilter = new Filter(filterName, criteria, selectedFilter.id);
      dispatch(updateFilter(newFilter));
    } else {
      const newFilter = new Filter(filterName, criteria);
      dispatch(saveFilter(newFilter));
    }
  };

  const handleClose = () => {
    if (closeModal) closeModal();
    setFilterModalValuesToDefault();
    dispatch(setSelectedFilter(null));
  };

  const setFilterModalValuesToDefault = () => {
    setFilterName('');
    setCriteria([defaultCriterion]);
  };

  const handleCriteriaChange = (event: ChangeEvent<HTMLSelectElement | HTMLInputElement>, criterionIndex: number) => {
    setCriteria((prevState) => {
      const newState = [...prevState];
      if (event.target.value === CriterionType.AMOUNT) {
        newState[criterionIndex] = {
          ...newState[criterionIndex],
          condition: AmountCriterionCondition.MORE,
          value: '0',
        };
      } else if (event.target.value === CriterionType.TITLE) {
        newState[criterionIndex] = {
          ...newState[criterionIndex],
          condition: TitleCriterionCondition.STARTS_WITH,
          value: '',
        };
      } else if (event.target.value === CriterionType.DATE) {
        newState[criterionIndex] = {
          ...newState[criterionIndex],
          condition: DateCriterionCondition.FROM,
          value: new Date().toDateString(),
        };
      }
      newState[criterionIndex] = { ...newState[criterionIndex], [event.target.name]: event.target.value };
      return newState;
    });
  };

  const handleDateChange = (date: Date, criterionIndex: number) => {
    setCriteria((prevState) => {
      const newState = [...prevState];
      newState[criterionIndex] = { ...newState[criterionIndex], value: date.toDateString() };
      return newState;
    });
  };

  const addCriterion = () => {
    setCriteria((prevState) => [...prevState, defaultCriterion]);
  };

  const deleteCriterion = (criterionIndex: number) => {
    setCriteria((prevState) => prevState.filter((_, i) => i !== criterionIndex));
  };

  const conditionSelect = (criterionType: CriterionType) => {
    if (criterionType === CriterionType.AMOUNT) {
      return Object.values(AmountCriterionCondition).map((amountCriterionCondition: AmountCriterionCondition) => (
        <option key={amountCriterionCondition}>{amountCriterionCondition}</option>
      ));
    } else if (criterionType === CriterionType.TITLE) {
      return Object.values(TitleCriterionCondition).map((titleCriterionCondition: TitleCriterionCondition) => (
        <option key={titleCriterionCondition}>{titleCriterionCondition}</option>
      ));
    } else {
      return Object.values(DateCriterionCondition).map((dateCriterionCondition: DateCriterionCondition) => (
        <option key={dateCriterionCondition}>{dateCriterionCondition}</option>
      ));
    }
  };

  return (
    <Form>
      <Form.Group className='filter-name-input'>
        <Form.Label>Filter name</Form.Label>
        <Form.Control onChange={(event) => setFilterName(event.target.value)} type='text' value={filterName} />
      </Form.Group>
      <Form.Group>
        <Form.Label>Criteria</Form.Label>
        {criteria.map((criterion: Criterion, index: number) => (
          <div key={index} className='criterion'>
            <Form.Select
              className='input'
              onChange={(event) => handleCriteriaChange(event, index)}
              value={criteria[index].type}
              name='type'
            >
              {Object.values(CriterionType).map((criterionType: CriterionType) => (
                <option key={criterionType}>{criterionType}</option>
              ))}
            </Form.Select>
            <Form.Select
              className='input'
              onChange={(event) => handleCriteriaChange(event, index)}
              value={criteria[index].condition}
              name='condition'
            >
              {conditionSelect(criterion.type)}
            </Form.Select>
            {criterion.type === CriterionType.DATE ? (
              <DatePicker
                className='input datepicker'
                selected={new Date(criteria[index].value)}
                dateFormat='dd/MM/yyyy'
                onChange={(event: Date) => handleDateChange(event, index)}
                customInput={<Form.Control />}
              />
            ) : (
              <Form.Control
                onChange={(event: ChangeEvent<HTMLInputElement>) => handleCriteriaChange(event, index)}
                value={criteria[index].value}
                type='text'
                name='value'
              />
            )}
            <Button className='delete-button' onClick={() => deleteCriterion(index)} disabled={criteria.length <= 1}>
              -
            </Button>
          </div>
        ))}
      </Form.Group>
      <div className='add-button'>
        <Button className='secondary-button' onClick={addCriterion}>
          + Add criterion
        </Button>
      </div>
      <div className='footer'>
        {closeModal && (
          <Button className='tertiary-button' onClick={handleClose}>
            Close
          </Button>
        )}
        <Button className='primary-button' onClick={handleSubmit}>
          Save
        </Button>
      </div>
    </Form>
  );
};

export default FilterForm;
