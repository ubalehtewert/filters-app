import { Modal } from 'react-bootstrap';

import { useAppSelector } from '../hooks.ts';

import FilterForm from './filter-form.tsx';

import '../styles/modal.scss';

type FilterModalProps = {
  show: boolean;
  handleClose: () => void;
};

const FilterModal = (props: FilterModalProps) => {
  const { show, handleClose } = props;
  const selectedFilter = useAppSelector((state) => state.filters.selectedFilter);

  return (
    <Modal show={show} dialogClassName='modal-dialog-scrollable'>
      <Modal.Header>
        <Modal.Title>{selectedFilter ? 'Edit filter' : 'Add new filter'}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <FilterForm closeModal={handleClose} />
      </Modal.Body>
    </Modal>
  );
};

export default FilterModal;
