import { Toaster } from 'sonner';

import Home from './pages/home.tsx';

const App = () => {
  return (
    <>
      <Home />
      <Toaster position='top-right' richColors closeButton />
    </>
  );
};

export default App;
