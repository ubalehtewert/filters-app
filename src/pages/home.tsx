import { useState } from 'react';
import { Button, Form } from 'react-bootstrap';

import FilterModal from '../components/filter-modal.tsx';
import FilterTable from '../components/filter-table.tsx';
import HomeFilterForm from '../components/home-filter-form.tsx';
import { useAppDispatch, useAppSelector } from '../hooks.ts';
import { setSelectedFilter } from '../slices/filters.slice.ts';

import '../styles/home.scss';

const Home = () => {
  const dispatch = useAppDispatch();
  const selectedFilter = useAppSelector((state) => state.filters.selectedFilter);
  const [isPopUp, setIsPopUp] = useState(true);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const openModal = () => {
    dispatch(setSelectedFilter(null));
    if (isPopUp) handleShow();
  };

  return (
    <>
      <div className='header'>
        <div className='title-checkbox'>
          <h1>Filters</h1>
          <Form.Check type='switch' label='Modal' checked={isPopUp} onChange={() => setIsPopUp(!isPopUp)} />
        </div>
        {(selectedFilter || isPopUp) && (
          <Button className='primary-button' onClick={openModal}>
            Add new filter
          </Button>
        )}
      </div>
      <div className='filters-with-form'>
        <FilterTable handleShow={handleShow} isPopUp={isPopUp} />
        {!isPopUp && <HomeFilterForm />}
      </div>
      <FilterModal show={show} handleClose={handleClose} />
    </>
  );
};

export default Home;
