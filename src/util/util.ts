import { AxiosError } from 'axios';

export const handleError = (error: AxiosError) => {
  const errorResponse = error.response;
  if (errorResponse && typeof errorResponse.data === 'string') {
    throw Error(errorResponse.data);
  } else if (
    errorResponse &&
    Array.isArray(errorResponse.data) &&
    errorResponse.data.length > 0 &&
    typeof errorResponse.data[0] === 'string'
  ) {
    throw Error(errorResponse.data[0]);
  } else {
    throw new Error('An error occurred: No response received');
  }
};
