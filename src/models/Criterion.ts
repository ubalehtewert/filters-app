import { CriterionType } from '../constants/enums.ts';

class Criterion {
  id?: number;
  type: CriterionType;
  condition: string;
  value: string;

  constructor(type: CriterionType, condition: string, value: string, id?: number) {
    this.id = id;
    this.type = type;
    this.condition = condition;
    this.value = value;
  }
}

export default Criterion;
