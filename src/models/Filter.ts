import Criterion from './Criterion.ts';

class Filter {
  id?: number;
  name: string;
  criteria: Criterion[];

  constructor(name: string, criteria: Criterion[], id?: number) {
    this.id = id;
    this.name = name;
    this.criteria = criteria;
  }
}

export default Filter;
