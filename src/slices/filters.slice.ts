import { AnyAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { toast } from 'sonner';

import Filter from '../models/Filter.ts';
import { deleteById, fetchAll, save, update } from '../services/filters.service.ts';

interface FiltersSlice {
  filters: Filter[];
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  saveStatus: 'idle' | 'loading' | 'succeeded' | 'failed';
  selectedFilter: Filter | null;
}

const initialState: FiltersSlice = {
  filters: [],
  status: 'idle',
  saveStatus: 'idle',
  selectedFilter: null,
};

export const getAllFilters = createAsyncThunk('filters/getAllFilters', async () => {
  return (await fetchAll()).data;
});

export const saveFilter = createAsyncThunk('filters/saveFilter', async (filter: Filter) => {
  return (await save(filter)).data;
});

export const updateFilter = createAsyncThunk('filters/updateFilter', async (filter: Filter) => {
  return (await update(filter)).data;
});

export const deleteFilterById = createAsyncThunk('filters/deleteFilterById', async (filterId: number) => {
  return (await deleteById(filterId)).data;
});

export const filtersSlice = createSlice({
  name: 'filters',
  initialState,
  reducers: {
    setSaveStatusIdle: (state) => {
      state.saveStatus = 'idle';
    },
    setSelectedFilter: (state, action) => {
      state.selectedFilter = action.payload;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(getAllFilters.fulfilled, (state, action) => {
        state.filters = action.payload;
        state.status = 'succeeded';
      })
      .addCase(getAllFilters.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(saveFilter.fulfilled, (state, action) => {
        state.filters.push(action.payload);
        state.saveStatus = 'succeeded';
        toast.success('Filter saved successfully');
      })
      .addCase(saveFilter.rejected, (state) => {
        state.saveStatus = 'failed';
      })
      .addCase(updateFilter.fulfilled, (state, action) => {
        state.filters = state.filters.map((filter: Filter) =>
          filter.id != action.payload.id ? filter : action.payload
        );
        state.saveStatus = 'succeeded';
        toast.success('Filter updated successfully');
      })
      .addCase(updateFilter.rejected, (state) => {
        state.saveStatus = 'failed';
      })
      .addCase(deleteFilterById.fulfilled, (state, action) => {
        state.filters = state.filters.filter((filter: Filter) => filter.id !== action.payload);
        toast.success('Filter deleted successfully');
      })
      .addMatcher(
        (action) => action.type.endsWith('/rejected'),
        (_state, action: AnyAction) => {
          toast.error(action.error.message);
        }
      );
  },
});

export const { setSaveStatusIdle, setSelectedFilter } = filtersSlice.actions;

export default filtersSlice.reducer;
