export enum CriterionType {
  AMOUNT = 'Amount',
  TITLE = 'Title',
  DATE = 'Date',
}

export enum AmountCriterionCondition {
  MORE = 'More than',
  LESS = 'Less than',
  EQUAL = 'Equal',
}

export enum TitleCriterionCondition {
  STARTS_WITH = 'Starts with',
  ENDS_WITH = 'Ends with',
  CONTAINS = 'Contains',
}

export enum DateCriterionCondition {
  FROM = 'From',
  UNTIL = 'Until',
  EQUAL = 'Equal',
}
